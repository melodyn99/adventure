(* Note: You may introduce new code anywhere in this file. *)

open Yojson.Basic.Util

type room_id = string
type exit_name = string
exception UnknownRoom of room_id
exception UnknownExit of exit_name
exception NonexistentDoor of room_id * room_id

(* [item] represents an item in the game. Has a name, the current room it is
    in, and its value. *)
type item = {
  names:string;
  room:string;
  points:int;
  description:string;
}

(* [door] represents a door blocking the exits in the adventure. Has an
    entryway, which contains the entrance room and exit door, and a state which
    is true if the door is unlocked and false if it is locked. *)
type door = {
  entryway:string list;
  unlocked:bool;
  key:string;
}

(* [exit] represents the exits for a room. Has a name and identifier. *)
type exit = {
  name:exit_name;
  id:room_id;
}

(* [room] represents a room in the adventure. Has an identifier, a
    description, and exits. *)
type room = {
  id:room_id;
  description1:string;
  description2:string;
  exits:exit list;
  score:int;
}

(* [t] represents an adventure. Has a list of rooms and a start room. *)
type t = {
  rooms:room list;
  start:room_id;
  items: item list;
  treasure_room:room_id;
  doors:door list;
  treasures:string list;
}

(** [exit_of_json json] is a helper function for room_of_json. Extracts all the
    information about exits - name and id - from each room. *)
let exit_of_json json = {
  name = json |> member "name" |> to_string;
  id = json |> member "room id" |> to_string;
}

(** [item_of_json json] is a helper function for from_json. Extracts all the
    information about each item - name, room identifier, and points - from
    the json. *)
let item_of_json json = {
  names = json |> member "name" |> to_string;
  room = json |> member "room" |> to_string;
  points = json |> member "points" |> to_string |> int_of_string;
  description = json |> member "description" |> to_string;
}
(** [door_of_json json] is a helper function for door_json. Extracts all the
    information about each door - entryways and state - from the json. *)
let door_of_json json = {
  entryway = (json |> member "entryway" |> to_string)::(json |> member "exit" |> to_string)::[];
  unlocked = json |> member "unlocked" |> to_string |> bool_of_string;
  key = json |> member "key" |> to_string;
}

(** [room_of_json json] is a helper function for from_json. Extracts all the
    information about each room - id, description, and exits - from the json. *)
let room_of_json json = {
  id = json |> member "id" |> to_string;
  description1 = json |> member "description1" |> to_string;
  description2 = json |> member "description2" |> to_string;
  exits = json |> member "exits" |> to_list |> List.map exit_of_json;
  score = json |> member "score" |> to_string |> int_of_string;
}

(** [treasure_of_json json] is a helper function for from_json. Extracts the
    item_name of a treasure. *)
let treasure_of_json json =
  json |> member "item_name" |> to_string

(* [from_json json] returns an adventure which contains all information from
    the json - a list of rooms and a start room. Precondition: the json is a
    valid *)
let from_json json = {
  rooms = json |> member "rooms" |> to_list |> List.map room_of_json;
  start = json |> member "start room" |> to_string;
  items = json |> member "items" |> to_list |> List.map item_of_json;
  treasure_room = json |> member "treasure" |> to_string;
  doors = json |> member "doors" |> to_list |> List.map door_of_json;
  treasures = json |> member "treasures" |> to_list |> List.map treasure_of_json;
}

(* [start_room adv] returns the id of the starting room in the adventure
    adv. *)
let start_room adv =
  adv.start

(* [get_rooms adv] returns the list of rooms in the adventure adv.*)
let get_rooms adv =
  adv.rooms

(* [get_doors adv] returns the list of doors in the adventure adv. *)
let get_doors adv =
  adv.doors

(* [get_treasures adv] returns a list of items that are considered the
    treasures. *)
let get_treasures adv =
  adv.treasures

(* [door_legal room1 room2 lst] returns whether it is legal to move between [room1]
    and [room2]. If a door does not exist between the rooms, returns true.*)
let rec door_legal room1 room2 lst =
  match lst with
  | [] -> true
  | h::t ->
    if List.mem room1 h.entryway && List.mem room2 h.entryway
    then h.unlocked
    else door_legal room1 room2 t

(* [find_door room1 room2 lst] returns a door between [room1] and [room2]. If
    a door does not exist between the rooms, and a valid exit was taken, a
    NonexistentDoor exception is thrown. *)
let rec find_door room1 room2 lst =
  match lst with
  | [] -> raise (NonexistentDoor (room1, room2))
  | h::t ->
    if List.mem room1 h.entryway && List.mem room2 h.entryway
    then h
    else find_door room1 room2 t

(* [get_item_name item] returns the name of the item. *)
let get_item_name (item:item)=
  item.names

(* [get_item_room item] returns the room the item is located in. *)
let get_item_room (item:item) =
  item.room

(* [get_item_points item] returns the number of points the item is worth. *)
let get_item_points (item:item) =
  item.points

(* [get_item_descr item] returns the description of the item. *)
let get_item_descr (item:item) =
  item.description

exception NonexistentItem

(* [get_item] returns the item with the given item_name. *)
let rec get_item item_name item_list=
  match item_list with
  | [] -> raise NonexistentItem
  | h::t -> if h.names = item_name
            then h
            else get_item item_name t

(* [room_ids adv] returns a set-like list of all the room identifiers in the
    adventure adv. *)
let room_ids adv =
  let rec get_room_ids rooms acc =
    match rooms with
    | [] -> acc
    | h::t -> get_room_ids t (h.id::acc) in
  get_room_ids adv.rooms []

(** [search_room room_list room_id] is a recursive helper function. Given a
    room identifier, search_room searches through a set-like list of rooms and
    either returns the room with the same room identifier, or raises an
    UnknownRoom exception. *)
let rec search_room room_list room_id =
  match room_list with
  | [] -> raise (UnknownRoom room_id)
  | h::t ->
    if h.id = room_id
    then h
    else search_room t room_id

(* [get_score room_id] returns the score of the room. **)
let get_score adv room_id =
  let room = search_room (adv.rooms) room_id in
  room.score

(* [get_items adv] returns the list of items in the adventure adv. *)
let get_items adv =
  adv.items

(* [new_item item_name room points] creates a new item with the given item_name
    as its name, room as its current location, and points as its value. *)
let new_item item_name room points description =
  {
    names = item_name;
    room = room;
    points = points;
    description = description
  }

(* [get_key door] returns the string name of they key that unlocks it. *)
let get_key door =
  door.key

(* [new_door door] creates a new door with the same entryway and key as the
    given [door], and a new locked state.*)
let new_door door unlocked =
  {
    entryway = door.entryway;
    unlocked = unlocked;
    key = door.key;
  }

(* [description adv room_id] returns the description of the room with the
    given room identifer. Calls on the recursive helper function search_room. *)
let description adv room_id =
  (search_room adv.rooms room_id).
    description1

(* [description2 adv room_id] returns the second description of the room with the
    given room identifer. Calls on the recursive helper function search_room. *)
let description2 adv room_id =
  (search_room adv.rooms room_id).
    description2

(* [is_treasure_room adv room_id] returns true if the given room_id is the same
    as the treasure room. Returns false otherwise. *)
let is_treasure_room adv room_id =
  adv.treasure_room = room_id

(** [check_start_room room_list room_id] is a recursive helper function for [exits].
    Given a room_id, check_start_room searches through a set-like list of rooms and
    returns true if the start room_id is a room identifier in the given room_list. *)
let rec check_start_room room_list room_id =
  match room_list with
  | [] -> false
  | h::t ->
    if h.id = room_id
    then true
    else check_start_room t room_id

(* [return_exits rooms room_id] is a recursive helper function for [exits].
    Given a list of rooms and a room_id, return_exits returns the list of
    exits of the room_id.
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
let rec return_exits rooms room_id =
  match rooms with
  | [] -> raise (UnknownRoom room_id)
  | h::t ->
    if h.id = room_id
    then h.exits
    else return_exits t room_id

(* [exits a r] is a set-like list of all exit names from room [r] in
    adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
let exits adv room_id =
  let rec parse_exits exits acc =
    match exits with
    | [] -> acc
    | h::t ->
      if List.mem h.name acc
      then parse_exits t acc
      else parse_exits t (h.name::acc) in
  if check_start_room adv.rooms room_id then
    parse_exits (return_exits adv.rooms room_id) []
  else raise (UnknownRoom room_id)

(** [check_exit exits ex_name] is a recursive helper function that checks
    if the exit with name ex_name is an element of the list of exit names. *)
let rec check_exit exits ex_name =
  match exits with
  | [] -> false
  | h::t ->
    if h = ex_name
    then true
    else check_exit t ex_name

(* [return_exit_id exits ex_name] is a recursive helper function that returns
    the room_id of the exit with the given name ex_name. *)
let rec return_exit_id exits ex_name =
  match exits with
  | [] -> raise (UnknownExit ex_name)
  | h::t ->
    if h.name = ex_name
    then h.id
    else return_exit_id t ex_name

(* [next_room a r e] is the room to which [e] exits from room [r] in
    adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a].
    Raises [UnknownExit e] if [e] is not an exit from room [r] in [a]. *)
let next_room adv room_id ex_name =
  if check_start_room adv.rooms room_id = false then
    raise (UnknownRoom room_id)
  else if check_exit (exits adv room_id) ex_name = false then
    raise (UnknownExit ex_name)
  else return_exit_id (return_exits adv.rooms room_id) ex_name

(* [next_rooms a r] is a set-like list of all rooms to which there is an exit
    from [r] in adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a].*)
let next_rooms adv room_id =
  let rec parse_exitsID (exits:exit list) (acc:room_id list) =
    match exits with
    | [] -> acc
    | h::t ->
      if List.mem h.id acc
      then parse_exitsID t acc
      else parse_exitsID t (h.id::acc) in
  if check_start_room adv.rooms room_id then
    parse_exitsID (return_exits adv.rooms room_id) []
  else raise (UnknownRoom room_id)
