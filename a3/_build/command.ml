(* Note: You may introduce new code anywhere in this file. *)

type object_phrase = string list

type command =
  | Go of object_phrase
  | Take of object_phrase
  | Drop of object_phrase
  | Inventory
  | Score
  | Unlock of object_phrase
  | Lock of object_phrase
  | Quit

exception Empty

exception Malformed

(** [delete_spaces lst acc] is a recursive helper function for [parse].
    Given a list and accumulator, it returns a list with all the empty strings
    removed from the original list. *)
let rec delete_spaces lst acc =
  match lst with
  |[] -> acc
  |h::t ->
    if h = ""
    then delete_spaces t acc
    else delete_spaces t (h::acc)

(* [parse str] is a function that parses a player's input into a command. This function
    takes the string given and makes the first word the verb, and the collection
    of the following words the object phrase.
    Raises [Empty] if the string is the empty string or
    contains only spaces.
    Raises [Malformed]  if the command is malformed, in which malformed can
    be defined as:
    - if the verb is neither "quit" or "go" or "score"
    - if the verb is "quit" and there is a non-empty object phrase
    - if the verb is "go" and there is an empty object phrase. *)
let parse str =
  let str_list = List.rev (delete_spaces (String.split_on_char ' ' str) []) in
  match str_list with
  |[] -> raise Empty
  |h::[] ->
    if h = "quit" then Quit
    else if h = "score" then Score
    else if h = "inventory" then Inventory
    else raise Malformed
  |h::t ->
    if h = "go" then Go t
    else if h = "drop" then Drop t
    else if h = "take" then Take t
    else if h = "unlock" then Unlock t
    else if h = "lock" then Lock t
    else raise Malformed

