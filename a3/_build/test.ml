open OUnit2
open Adventure
open Command
open State

(********************************************************************
   Here are some helper functions for your testing of set-like lists.
 ********************************************************************)

(** [cmp_set_like_lists lst1 lst2] compares two lists to see whether
    they are equivalent set-like lists.  That means checking two things.
    First, they must both be {i set-like}, meaning that they do not
    contain any duplicates.  Second, they must contain the same elements,
    though not necessarily in the same order. *)
let cmp_set_like_lists lst1 lst2 =
  let uniq1 = List.sort_uniq compare lst1 in
  let uniq2 = List.sort_uniq compare lst2 in
  List.length lst1 = List.length uniq1
  &&
  List.length lst2 = List.length uniq2
  &&
  uniq1 = uniq2

(** [pp_string s] pretty-prints string [s]. *)
let pp_string s = "\"" ^ s ^ "\""

(** [pp_list pp_elt lst] pretty-prints list [lst], using [pp_elt]
    to pretty-print each element of [lst]. *)
let pp_list pp_elt lst =
  let pp_elts lst =
    let rec loop n acc = function
      | [] -> acc
      | [h] -> acc ^ pp_elt h
      | h1::(h2::t as t') ->
        if n=100 then acc ^ "..."  (* stop printing long list *)
        else loop (n+1) (acc ^ (pp_elt h1) ^ "; ") t'
    in loop 0 "" lst
  in "[" ^ pp_elts lst ^ "]"

(* These tests demonstrate how to use [cmp_set_like_lists] and
   [pp_list] to get helpful output from OUnit. *)
let cmp_demo =
  [
    "order is irrelevant" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["foo"; "bar"] ["bar"; "foo"]);
    (* Uncomment this test to see what happens when a test case fails. *)
    (* "duplicates not allowed" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["foo"; "foo"] ["foo"]);  *)

  ]

(********************************************************************
   End helper functions.
 ********************************************************************)

(* You are welcome to add strings containing JSON here, and use them as the
   basis for unit tests.  Or you can add .json files in this directory and
   use them, too.  Any .json files in this directory will be included
   by [make zip] as part of your CMS submission. *)

let lonely = Yojson.Basic.from_file "lonely_room.json"
let ho_plaza = Yojson.Basic.from_file "ho_plaza.json"
let lonely_adv = from_json lonely
let ho_plaza_adv = from_json ho_plaza
let adventure_tests =
  [
    "test start room 1" >:: (fun _ ->
        assert_equal "the room" (start_room lonely_adv));
    "test start room 2" >:: (fun _ ->
        assert_equal "ho plaza" (start_room ho_plaza_adv));
    "test room_id 1" >:: (fun _ ->
        assert_equal ["the room"] (room_ids lonely_adv) ~printer:(pp_list pp_string));
    "test room_id 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["ho plaza"; "health"; "tower"; "nirvana"] (room_ids ho_plaza_adv));
    "test description 1" >:: (fun _ ->
        assert_equal "A very lonely room." (description lonely_adv "the room"));
    "test description 2" >:: (fun _ ->
        assert_equal "You are on Ho Plaza. Cornell Health is to the southwest. The chimes are playing a concert in the clock tower. Someone tries to hand you a quartercard, but you avoid them."
          (description ho_plaza_adv "ho plaza"));
    "test description 3" >:: (fun _ ->
        assert_equal "You are at the entrance to Cornell Health. A sign advertises free flu shots. You briefly wonder how long it would take to get an appointment. Ho Plaza is to the northeast."
          (description ho_plaza_adv "health"));
    "test description 4" >:: (fun _ ->
        assert_equal "You climbed up all 161 steps to the top of McGraw Tower. A Chimesmaster is playing the Jennie McGraw Rag. You feel inspired to ascend higher."
          (description ho_plaza_adv "tower"));
    "test description 5" >:: (fun _ ->
        assert_equal "You have reached a higher level of existence.  There are no more words."
          (description ho_plaza_adv "nirvana"));
    "test exits 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          [] (exits lonely_adv "the room"));
    "test exits 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["southwest"; "south west"; "Cornell Health"; "Gannett"; "chimes"; "concert"; "clock tower"]
          (exits ho_plaza_adv "ho plaza"));
    "test exits 3" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["northeast"; "north east"; "Ho Plaza"] (exits ho_plaza_adv "health"));
    "test exits 4" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["down"; "back"; "Ho Plaza"; "higher"] (exits ho_plaza_adv "tower"));
    "test exits 5" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          [] (exits ho_plaza_adv "nirvana"));
    "test exits 6" >:: (fun _ ->
        assert_raises (UnknownRoom "Duffield") (fun() -> exits ho_plaza_adv "Duffield"));
    "test next_room 1" >:: (fun _ ->
        assert_equal "health"
          (next_room ho_plaza_adv "ho plaza" "southwest"));
    "test next_room 2" >:: (fun _ ->
        assert_equal "tower"
          (next_room ho_plaza_adv "ho plaza" "concert"));
    "test next_room 3" >:: (fun _ ->
        assert_equal "ho plaza"
          (next_room ho_plaza_adv "health" "Ho Plaza"));
    "test next_room 4" >:: (fun _ ->
        assert_equal "ho plaza"
          (next_room ho_plaza_adv "health" "north east"));
    "test next_room 5" >:: (fun _ ->
        assert_equal "ho plaza"
          (next_room ho_plaza_adv "health" "northeast"));
    "test next_room 6" >:: (fun _ ->
        assert_raises (UnknownExit "Duffield") (fun() -> next_room ho_plaza_adv "health" "Duffield"));
    "test next_room 7" >:: (fun _ ->
        assert_raises (UnknownRoom "Duffield") (fun() -> next_room ho_plaza_adv "Duffield" "northeast"));
    "test next_rooms 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["health"; "tower"] (next_rooms ho_plaza_adv "ho plaza"));
    "test next_rooms 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["ho plaza"] (next_rooms ho_plaza_adv "health"));
    "test next_rooms 3" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["ho plaza"; "nirvana"] (next_rooms ho_plaza_adv "tower"));
    "test next_rooms 4" >:: (fun _ ->
        assert_raises (UnknownRoom "Duffield") (fun() -> next_rooms ho_plaza_adv "Duffield"));
  ]

let command_tests =
  [
    "test parse 1" >:: (fun _ ->
        assert_equal (Go ["clock";"tower"]) (parse "go clock tower"));
    "test parse 2" >:: (fun _ -> assert_raises (Malformed) (fun() -> parse "GO"));
    "test parse 3" >:: (fun _ ->
        assert_equal (Go ["clock";"tower"]) (parse "go       clock    tower"));
    "test parse 4" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "Go clock tower"));
    "test parse 5" >:: (fun _ ->
        assert_equal (Quit) (parse "quit"));
    "test parse 6" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "quit clock tower"));
    "test parse 7" >:: (fun _ ->
        assert_raises (Empty) (fun() -> parse " "));
    "test parse 8" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "Quit"));
  ]

let initial_ho_plaza = init_state ho_plaza_adv
let ho_plaza_state1 = get_state (go "southwest" ho_plaza_adv initial_ho_plaza)
let ho_plaza_state2 = get_state (go "concert" ho_plaza_adv initial_ho_plaza)
let state_tests =
  [
    "test current_room_id 2" >:: (fun _ ->
        assert_equal "ho plaza" (current_room_id initial_ho_plaza));
    "test current_room_id 2" >:: (fun _ ->
        assert_equal "health" (current_room_id ho_plaza_state1));
    "test current_room_id 3" >:: (fun _ ->
        assert_equal "tower" (current_room_id ho_plaza_state2));
    "test visited" >:: (fun _ ->
        assert_equal ["ho plaza"] (visited initial_ho_plaza));
  ]

let suite =
  "test suite for A2"  >::: List.flatten [
    adventure_tests;
    command_tests;
    state_tests;
  ]

let _ = run_test_tt_main suite
