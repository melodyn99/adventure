(**
   Representation of dynamic adventure state.

   This module represents the state of an adventure as it is being played,
   including the adventurer's current room, the rooms that have been visited,
   and functions that cause the state to change.
*)

(* You are free to add more code here. *)

(**********************************************************************
 * DO NOT CHANGE THIS CODE
 * It is part of the interface the course staff will use to test your
 * submission.
*)

(** The abstract type of values representing the game state. *)
type t

(** [init_state a] is the initial state of the game when playing adventure [a].
    In that state the adventurer is currently located in the starting room,
    and they have visited only that room. *)
val init_state : Adventure.t -> t

(** [current_room_id st] is the identifier of the room in which the adventurer
    currently is located in state [st]. *)
val current_room_id : t -> string

(** [visited st] is a set-like list of the room identifiers the adventurer has
    visited in state [st]. The adventurer has visited a room [rm] if their
    current room location is or has ever been [rm]. *)
val visited : t -> string list

(** The type representing the result of an attempted movement. *)
type result = Legal of t | Illegal

(** [go exit adv st] is [r] if attempting to go through exit [exit] in state
    [st] and adventure [adv] results in [r].  If [exit] is an exit from the
    adventurer's current room, then [r] is [Legal st'], where in [st'] the
    adventurer is now located in the room to which [exit] leads.  Otherwise,
    the result is [Illegal].
    Effects: none.  [go] is not permitted to do any printing. *)
val go : Adventure.exit_name -> Adventure.t -> t -> result

(* END DO NOT CHANGE
 **********************************************************************)

(* You are free to add more code here. *)

(** Raised when a nonexistent item is encountered. *)
exception NonexistentItem

(** Raised when player encounters a locked door. *)
exception DoorIsLocked of string

(** Raised when player attempts to unlock or lock a door without the appropriate
    key. *)
exception NoKey of string

(** [get_state res] returns the state of the result if the result is legal.
    Otherwise, returns an empty unreachable state. *)
val get_state: result -> t

(**[get_score st] returns the current score of the game state. *)
val get_score: t -> int

(** [take adv st item_name] transfers an item from a room to the adventurer's
    inventory. If the item called is not in the room or does not exist, then
    the result is [Illegal]. *)
val take: Adventure.t -> t -> string -> result

(** [drop adv st item_name] transfers an item from the adventurer's inventory
    to the current room they are in. If the item is not in their inventory or
    does not exist, then the result is [Illegal]. *)
val drop: Adventure.t -> t -> string -> result

(** [inventory st] returns a string of all the items currently in the
    adventurer's inventory. *)
val inventory: t -> string

(** [unlock exit_name adv st] unlocks the door with the given [exit_name] and
    the current room of [st] by changing the value unlocked to true. If the
    given [exit_name] is not an exit of the room or does not exist, then the
    result is [Illegal]. *)
val unlock: string -> Adventure.t -> t -> result

(** [lock exit_name adv st] locks the door with the given [exit_name] and
    the current room of [st] by changing the value unlocked to true. Raises
    NoKey if the player does not have the key to lock the door. If the
    given [exit_name] is not an exit of the room or does not exist, then the
    result is [Illegal]. *)
val lock: string -> Adventure.t -> t -> result

(** [has_won st] returns true if all the items in the given state are in the
    treasure room and false otherwise. *)
val has_won: t -> Adventure.t -> bool

(** [has_visited] returns whether the room identifier is currently in the
    list of rooms visited in st. *)
val has_visited: t -> string -> bool

(** [get_items_in_room] returns a string containing all the items in the
    current room of the state. *)
val get_items_in_room: t -> string

(** [get_items st] returns the list of items in the game. *)
val get_items: t -> Adventure.item list