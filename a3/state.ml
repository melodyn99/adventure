(* Note: You may introduce new code anywhere in this file. *)

(** [item] represents an item in the game. Has a name, the current room it is
    in, and its value.*)
type item = {
  name:string;
  room:string;
  points:int;
}

(* The abstract type of values representing the game state. *)
type t = {
  curr_room: string;
  visited: string list;
  curr_score: int;
  inventory: string list;
  items: Adventure.item list;
  doors: Adventure.door list;
  treasures: string list;
}

(* [init_state a] is the initial state of the game when playing adventure [a].
    In that state the adventurer is currently located in curr_room, and visited is
    the list of visited rooms. *)
let init_state adv = {
  curr_room = Adventure.start_room adv;
  visited = [Adventure.start_room adv];
  curr_score = Adventure.get_score adv (Adventure.start_room adv);
  inventory = [];
  items = Adventure.get_items adv;
  doors = Adventure.get_doors adv;
  treasures = Adventure.get_treasures adv;
}

(* [current_room_id st] is the identifier of the room in which the adventurer
    currently is located in state [st]. *)
let current_room_id st =
  st.curr_room

(* [visited st] is a set-like list of the room identifiers the adventurer has
    visited in state [st]. The adventurer has visited a room [rm] if their
    current room location is or has ever been [rm]. *)
let visited st =
  st.visited

(* [get_score st] returns the current score of the game state. *)
let get_score st =
  st.curr_score

(** [get_inventory st] returns the list of items in the adventurer's inventory. *)
let get_inventory st =
  st.inventory

(* [get_items st] returns the list of items in the game. *)
let get_items st =
  st.items

exception NonexistentItem

(** [get_room item_list item_name] returns the room identifier containing the
    item name. *)
let rec get_room (item_list:Adventure.item list) item_name =
  match item_list with
  | [] -> raise NonexistentItem
  | h::t -> if (Adventure.get_item_name h) = item_name
    then Adventure.get_item_room h
    else get_room t item_name

(** [get_room item_list item_name] returns the room identifier containing the
    item name. *)
let rec get_points (item_list:Adventure.item list) item_name =
  match item_list with
  | [] -> raise NonexistentItem
  | h::t -> if (Adventure.get_item_name h) = item_name
    then Adventure.get_item_points h
    else get_points t item_name

(** [remove_item item_list acc item_name] removes the item with the given
    item_name from the list of all the items in the adventure. *)
let rec remove_item item_list acc item_name =
  match item_list with
  | [] -> acc
  | h::t -> if (Adventure.get_item_name h) = item_name
    then remove_item t acc item_name
    else remove_item t (List.cons h acc) item_name

(** [remove_inventory_item item_list acc item_name] removes the item with the
    given item_name from the adventurer's inventory. *)
let rec remove_inventory_item string_list acc item_name =
  match string_list with
  | [] -> acc
  | h::t -> if h = item_name
    then remove_inventory_item t acc item_name
    else remove_inventory_item t (List.cons h acc) item_name

(** [change_door door_list acc door rep_door locked] removes the old [door] in the
    given [door_list] and replaces it with a new door with a new locked state. *)
let rec change_door door_list acc door unlocked =
  match door_list with
  | [] -> acc
  | h::t -> if h = door
    then change_door t (List.cons (Adventure.new_door door unlocked) acc) door unlocked
    else change_door t (List.cons h acc) door unlocked

(* [has_visited] returns whether the room identifier is currently in the
    list of rooms visited in st. *)
let has_visited st room_id =
  List.mem room_id (st.visited)

(* The type representing the result of an attempted movement. *)
type result = Legal of t | Illegal

(** [make_set_like lst room] is a helper function for [go]. It takes in a list
    and if the room given is not in the list, it appends room to the list.
    Otherwise, it returns the list. *)
let make_set_like lst room =
  if List.mem room lst
  then lst
  else room::lst

exception DoorIsLocked of string

exception NoKey of string

(* [unlock exit_name adv st] unlocks the door with the given [exit_name] and
    the current room of [st] by changing the value unlocked to true. Raises
    NoKey if the player does not have the key to unlock the door. If the
    given [exit_name] is not an exit of the room or does not exist, then the
    result is [Illegal]. *)
let unlock exit_name adv st =
  let exit_rooms = Adventure.exits adv st.curr_room in
  let door = Adventure.find_door (Adventure.next_room adv st.curr_room exit_name) st.curr_room st.doors in
  let door_key = Adventure.get_key door in
  if List.mem exit_name exit_rooms
  then if List.mem door_key st.inventory
    then Legal {
        curr_room = st.curr_room;
        curr_score = st.curr_score;
        visited = st.visited;
        inventory = st.inventory;
        items = st.items;
        doors = change_door st.doors [] (Adventure.find_door (Adventure.next_room adv st.curr_room exit_name) st.curr_room st.doors) true;
        treasures = st.treasures;
      }
    else raise (NoKey door_key)
  else Illegal

(* [lock exit_name adv st] locks the door with the given [exit_name] and
    the current room of [st] by changing the value unlocked to true. Raises
    NoKey if the player does not have the key to lock the door. If the
    given [exit_name] is not an exit of the room or does not exist, then the
    result is [Illegal]. *)
let lock exit_name adv st =
  let exit_rooms = Adventure.exits adv st.curr_room in
  let door = Adventure.find_door (Adventure.next_room adv st.curr_room exit_name) st.curr_room st.doors in
  let door_key = Adventure.get_key door in
  if List.mem exit_name exit_rooms
  then if List.mem door_key st.inventory
    then Legal {
        curr_room = st.curr_room;
        curr_score = st.curr_score;
        visited = st.visited;
        inventory = st.inventory;
        items = st.items;
        doors = change_door st.doors [] (Adventure.find_door (Adventure.next_room adv st.curr_room exit_name) st.curr_room st.doors) false;
        treasures = st.treasures;
      }
    else raise (NoKey door_key)
  else Illegal

(* [go exit adv st] is [r] if attempting to go through exit [exit] in state
    [st] and adventure [adv] results in [r].  If [exit] is an exit from the
    adventurer's current room, then [r] is [Legal st'], where in [st'] the
    adventurer is now located in the room to which [exit] leads.  Otherwise,
    the result is [Illegal].
    Effects: none.  [go] is not permitted to do any printing. *)
let go ex adv st =
  let exit_rooms = Adventure.exits adv st.curr_room in
  if List.mem ex exit_rooms && (Adventure.door_legal (Adventure.next_room adv st.curr_room ex) st.curr_room st.doors)
  then Legal {
      curr_room = Adventure.return_exit_id (Adventure.return_exits
                                              (Adventure.get_rooms adv) st.curr_room) ex;
      curr_score = if has_visited st (Adventure.return_exit_id (Adventure.return_exits
                                                                  (Adventure.get_rooms adv) st.curr_room) ex)
        then st.curr_score
        else st.curr_score + Adventure.get_score adv (Adventure.return_exit_id (Adventure.return_exits
                                                                                  (Adventure.get_rooms adv) st.curr_room) ex);
      visited = make_set_like st.visited (Adventure.return_exit_id
                                            (Adventure.return_exits (Adventure.get_rooms adv) st.curr_room) ex);
      inventory = st.inventory;
      items = st.items;
      doors = st.doors;
      treasures = st.treasures;
    }
  else if (Adventure.door_legal (Adventure.next_room adv st.curr_room ex) st.curr_room st.doors) = false
  then raise (DoorIsLocked (Adventure.get_key (Adventure.find_door (Adventure.next_room adv st.curr_room ex) st.curr_room st.doors)))
  else Illegal

(* [take adv st item_name] transfers an item from a room to the adventurer's
    inventory. If the item called is not in the room or does not exist, then
    the result is [Illegal]. *)
let take adv st (item_name:string) =
  if (st.curr_room = (get_room st.items item_name))
  then Legal {
      curr_room = st.curr_room;
      curr_score =
        if Adventure.is_treasure_room adv st.curr_room
        then st.curr_score - (Adventure.get_item_points (Adventure.get_item item_name st.items))
        else st.curr_score;
      visited = st.visited;
      inventory = item_name::(st.inventory);
      items = (Adventure.new_item item_name "inventory" (get_points st.items item_name)
                 (Adventure.get_item_descr (Adventure.get_item item_name st.items)))::(remove_item st.items [] item_name);
      doors = st.doors;
      treasures = st.treasures;
    }
  else Illegal

(** [list_items list] is a recursive helper function that determines whether
    to insert a comma or "and" in a string. *)
let rec list_items list =
  match list with
  | [] -> ""
  | l::[] -> "and " ^ l
  | h::t -> h ^ ", " ^ list_items t

(* [inventory st] returns a string of all the items currently in the
       adventurer's inventory. *)
let inventory st =
  match st.inventory with
  | [] -> ""
  | l::[] -> String.concat "" st.inventory
  | h1::h2::[] -> String.concat " and " st.inventory
  | h::t -> list_items st.inventory

(** [parse_inventory inventory_list acc room] is a recursive helper function
    that returns a list of item names in the given room. *)
let rec parse_inventory inventory_list acc room =
  match inventory_list with
  | [] -> acc
  | h::t ->
    if Adventure.get_item_room h = room
    then parse_inventory t ((Adventure.get_item_name h)::acc) room
    else parse_inventory t acc room

(* [get_items_in_room] returns a string containing all the items in the
    current room of the state. *)
let get_items_in_room st =
  let list = parse_inventory st.items [] st.curr_room in
  match list with
  | [] -> ""
  | l::[] -> String.concat "" list
  | h1::h2::[] -> String.concat " and " list
  | h::t -> list_items list

(* [drop adv st item_name] transfers an item from the adventurer's inventory
    to the current room they are in. If the item is not in their inventory or
    does not exist, then the result is [Illegal]. *)
let drop adv st item_name =
  if List.mem item_name (st.inventory)
  then Legal {
      curr_room = st.curr_room;
      curr_score =
        if Adventure.is_treasure_room adv st.curr_room
        then st.curr_score + (Adventure.get_item_points (Adventure.get_item item_name st.items))
        else st.curr_score;
      visited = st.visited;
      inventory = remove_inventory_item st.inventory [] item_name;
      items = (Adventure.new_item item_name (st.curr_room) (get_points st.items item_name)
                 (Adventure.get_item_descr (Adventure.get_item item_name st.items)))::(remove_item st.items [] item_name);
      doors = st.doors;
      treasures = st.treasures;
    }
  else Illegal

(** [check_items treasure_names items adv] is a recursive helper function that checks whether
    all the items are in the treasure room, given the list of treasure_names and items. *)
let rec check_items treasure_names items adv =
  match items with
  | [] -> true
  | h::t ->
    if List.mem (Adventure.get_item_name h) treasure_names
    then
      if Adventure.is_treasure_room adv (Adventure.get_item_room h)
      then check_items treasure_names t adv
      else false
    else check_items treasure_names t adv

(* [has_won st] returns true if all the items in the given state are in the
    treasure room and false otherwise. *)
let has_won st adv =
  check_items st.treasures st.items adv

(* [get_state res] returns the state of the result if the result is legal.
    Otherwise, returns an empty unreachable state. *)
let get_state res =
  match res with
  |Legal (t) -> t
  |Illegal -> {curr_room = ""; visited = []; curr_score = 0; inventory = [];
               items = []; doors = []; treasures = []}
