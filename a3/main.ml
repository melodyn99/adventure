open Yojson.Basic.Util
(** [play_game f] starts the adventure in file [f]. *)

(** [run_command input state adv] is a recursive helper function. Takes in an
    input which is parsed into a command. Then it applies the command to the
    current state and prompts the user for a new command on the new state.
    Exits the program with the input "quit."*)
let rec run_command input state adv =
  try match (Command.parse input) with
    |Command.Quit -> print_endline "Goodbye player!"; exit 0
    |Command.Score -> print_endline ("Score: " ^ (State.get_score state |> string_of_int));
      print_string  "> ";
      run_command (read_line ()) state adv
    |Command.Inventory ->
      if State.inventory state = ""
      then print_endline ("You currently have no items in your possession.")
      else
        print_endline ("You currently have a " ^ State.inventory state ^ " in your possession.");
      print_string  "> ";
      run_command (read_line ()) state adv
    |Command.Go (lst) -> (
        let result = State.go (String.concat " " lst) adv state in
        match result with
        |Legal (st) ->
          if State.has_visited state (State.current_room_id st)
          then
            let items = State.get_items_in_room st in
            print_endline (Adventure.description2 adv (State.current_room_id st) ^
            if items = ""
            then " There doesn't seem to be any items here."
            else " As you prepare to leave, you spot a " ^ items ^ " on the floor.");
            print_string  "> ";
            run_command (read_line ()) st adv
          else print_endline (Adventure.description adv (State.current_room_id st));
                print_string  "> ";
                run_command (read_line ()) st adv
        |Illegal -> print_endline "Please type in a valid exit.";
          print_string  "> ";
          run_command (read_line ()) state adv)
    |Command.Take (lst) -> (
        let item = String.concat " " lst in
        let descr = Adventure.get_item_descr (Adventure.get_item item (State.get_items state)) in
        let result = State.take adv state item in
        match result with
        |Legal (st) ->
          print_endline ("You have collected a " ^ item ^ ". " ^ descr);
          print_string  "> ";
          run_command (read_line ()) st adv
        |Illegal -> print_endline ("There is no item called " ^ item ^ " here.");
          print_string  "> ";
          run_command (read_line ()) state adv)
    |Command.Unlock (lst) -> (
        let exit_name = String.concat " " lst in
        let result = State.unlock exit_name adv state in
        match result with
        |Legal (st) -> print_endline ("You have unlocked the door.");
          print_string  "> ";
          run_command (read_line ()) st adv
        |Illegal -> print_endline ("There is no door to unlock.");
          print_string  "> ";
          run_command (read_line ()) state adv)
    |Command.Lock (lst) -> (
        let exit_name = String.concat " " lst in
        let result = State.lock exit_name adv state in
        match result with
        |Legal (st) -> print_endline ("You have locked the door.");
          print_string  "> ";
          run_command (read_line ()) st adv
        |Illegal -> print_endline ("There is no door to lock.");
          print_string  "> ";
          run_command (read_line ()) state adv)
    |Command.Drop (lst) ->
      let item = String.concat " " lst in
      let result = State.drop adv state item in
      match result with
      |Legal (st) ->
        print_endline ("You have dropped " ^ item ^ " in " ^ State.current_room_id st ^ ".");
        if State.has_won st adv
        then (print_endline ("Congratulations! You have collected all the items!");
              print_endline ("Score: " ^ (State.get_score st |> string_of_int));
              exit 0)
        else print_string  "> ";
        run_command (read_line ()) st adv
      |Illegal -> print_endline ("There is no item called " ^ item ^ " to drop.");
        print_string  "> ";
        run_command (read_line ()) state adv
  with
  | Command.Empty -> print_endline "You did not type in a command.";
    print_string  "> ";
    run_command (read_line ()) state adv
  | Command.Malformed -> print_endline "Please type in a valid command.";
    print_string  "> ";
    run_command (read_line ()) state adv
  | State.NonexistentItem -> print_endline "Please type in a valid item.";
    print_string  "> ";
    run_command (read_line ()) state adv
  | Adventure.NonexistentItem -> print_endline "Please type in a valid item.";
    print_string  "> ";
    run_command (read_line ()) state adv
  | State.DoorIsLocked (key)-> print_endline ("This door is locked. Please find a "^ key ^ " to unlock it.");
    print_string  "> ";
    run_command (read_line ()) state adv
  | Adventure.UnknownExit (arg) -> print_endline "Please type in a valid exit.";
    print_string  "> ";
    run_command (read_line ()) state adv
  | Adventure.NonexistentDoor (room1, room2) -> print_endline ("There is no door between " ^ room1 ^ " and " ^ room2);
    print_string  "> ";
    run_command (read_line ()) state adv
  | State.NoKey (key)->
      if key = ""
      then (print_endline ("You cannot lock or unlock this door.");
        print_string  "> ";
        run_command (read_line ()) state adv)
      else print_endline ("The " ^ key ^ " is required to lock or unlock this door.");
        print_string  "> ";
        run_command (read_line ()) state adv

(** [play_game f] takes in a file name to start the game. *)
let play_game f =
  let j = Yojson.Basic.from_file f in
  let adv = Adventure.from_json j in
  let curr_st = State.init_state adv in
  print_endline (Adventure.description adv (Adventure.start_room adv));
  print_string "> ";
  match read_line () with
  | exception End_of_file -> ()
  | input_string -> run_command input_string curr_st adv

(** [parse_string ()] checks to make sure an appropriate game file is entered.
    If it is not, then it prompts the player to input another file name. *)
let rec parse_string () =
  try match read_line() with
  | exception End_of_file -> ()
  | file_name -> play_game file_name
  with
  | Yojson.Json_error (arg) -> print_endline "Please type in a valid game file.";
    print_string "> ";
    parse_string ()
  | Sys_error (arg) -> print_endline "Please type in a valid game file.";
    print_string "> ";
    parse_string ()
  | Yojson.Basic.Util.Type_error (arg1, arg2) -> print_endline "Please type in a valid game file.";
    print_string "> ";
    parse_string ()

(** [main ()] prompts for the game to play, then starts it. *)
let main () =
  ANSITerminal.(print_string [red]
                  "\n\nWelcome to the 3110 Text Adventure Game engine.\n");
  print_endline "Please enter the name of the game file you want to load.\n";
  print_string  "> ";
  parse_string ()

(* Execute the game engine. *)
let () = main ()