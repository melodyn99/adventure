(**
   Representation of static adventure data.

   This module represents the data stored in adventure files, including
   the rooms and exits.  It handles loading of that data from JSON as well
   as querying the data.
*)

(* You are free to add more code here. *)
(**********************************************************************
 * DO NOT CHANGE THIS CODE
 * It is part of the interface the course staff will use to test your
 * submission.
*)

(** The abstract type of values representing adventures. *)
type t

(** The type of room identifiers. *)
type room_id = string

(** The type of exit names. *)
type exit_name = string

(** Raised when an unknown room is encountered. *)
exception UnknownRoom of room_id

(** Raised when an unknown exit is encountered. *)
exception UnknownExit of exit_name

(** [from_json j] is the adventure that [j] represents.
    Requires: [j] is a valid JSON adventure representation. *)
val from_json : Yojson.Basic.json -> t

(** [start_room a] is the identifier of the starting room in adventure
    [a]. *)
val start_room : t -> room_id

(** [room_ids a] is a set-like list of all of the room identifiers in
    adventure [a]. *)
val room_ids : t -> room_id list

(** [description a r] is the description of room [r] in adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
val description : t -> room_id -> string

(** [exits a r] is a set-like list of all exit names from room [r] in
    adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
val exits : t -> room_id -> exit_name list

(** [next_room a r e] is the room to which [e] exits from room [r] in
    adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a].
    Raises [UnknownExit e] if [e] is not an exit from room [r] in [a]. *)
val next_room : t -> room_id -> exit_name -> room_id

(** [next_rooms a r] is a set-like list of all rooms to which there is an exit
    from [r] in adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a].*)
val next_rooms : t -> room_id -> room_id list

(* END DO NOT CHANGE
 **********************************************************************)

(** [item] represents an item in the game. Has a name, the current room it is
    in, and its value. *)
type item = {
  names:string;
  room:string;
  points:int;
  description:string;
}

(** [door] represents a door blocking the exits in the adventure. Has an
    entryway, which contains the entrance room and exit door, and a state which
    is true if the door is unlocked and false if it is locked. *)
type door = {
  entryway:string list;
  unlocked:bool;
  key:string;
}

(** [exit] represents the exits for a room. Has a name and identifier. *)
type exit = {
  name:exit_name;
  id:room_id;
}

(** [room] represents a room in the adventure. Has an identifier, a
    description, and exits. *)
type room = {
  id:room_id;
  description1:string;
  description2:string;
  exits:exit list;
  score:int;
}

(** [return_exit_id exits ex_name] is a recursive helper function that returns
    the room_id of the exit with the given name ex_name. *)
val return_exit_id : exit list -> exit_name -> room_id

(** [return_exits rooms room_id] is a recursive helper function for [exits].
    Given a list of rooms and a room_id, return_exits returns the list of
    exits of the room_id.
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
val return_exits: room list -> room_id -> exit list

(** [get_rooms adv] returns the list of rooms in the adventure adv.*)
val get_rooms: t -> room list

(** [get_doors adv] returns the list of doors in the adventure adv. *)
val get_doors: t -> door list

(** [door_legal room1 room2 lst] returns whether it is legal to move between [room1]
    and [room2]. If a door does not exist between the rooms, and a valid exit was
    taken, returns true. *)
val door_legal: string -> string -> door list -> bool

(** [find_door room1 room2 lst] returns a door between [room1] and [room2]. If
    a door does not exist between the rooms, and a valid exit was taken, a
    NonexistentDoor exception is thrown. *)
val find_door: string -> string -> door list -> door

(**[get_score adv room_id] returns the score of the room given room_id in the
    adventure adv. **)
val get_score: t -> room_id -> int

(** [get_items adv] returns the list of items in the adventure adv. *)
val get_items: t -> item list

(** [get_item_name item] returns the name of the item. *)
val get_item_name: item -> string

(** [get_item_room item] returns the room the item is located in. *)
val get_item_room: item -> string

(** [get_item_points item] returns the number of points the item is worth. *)
val get_item_points: item -> int

(** [new_item item_name room points] creates a new item with the given item_name
    as its name, room as its current location, and points as its value. *)
val new_item: string -> string -> int -> string -> item

(** [new_door door] creates a new unlocked door with the same entryway
    as the given [door].*)
val new_door: door -> bool -> door

(** [is_treasure_room adv room_id] returns true if the given room_id is the same
    as the treasure room. Returns false otherwise. *)
val is_treasure_room: t -> room_id -> bool

(** Raised when a nonexistent item is encountered.*)
exception NonexistentItem

(** Raised when trying to exit through a non-existent door. *)
exception NonexistentDoor of room_id * room_id

(** [get_item] returns the item with the given item_name. *)
val get_item: string -> item list -> item

(** [description2 a r] is the description2 of room [r] in adventure [a].
    Raises [UnknownRoom r] if [r] is not a room identifier in [a]. *)
val description2 : t -> room_id -> string

(** [get_key door] returns the string name of they key that unlocks it. *)
val get_key: door -> string

(** [get_item_descr item] returns the description of the item. *)
val get_item_descr: item -> string

(** [get_treasures adv] returns a list of items that are considered the
    treasures. *)
val get_treasures: t -> string list
