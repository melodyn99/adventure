open OUnit2
open Adventure
open Command
open State

(********************************************************************
   Here are some helper functions for your testing of set-like lists.
 ********************************************************************)

(** [cmp_set_like_lists lst1 lst2] compares two lists to see whether
    they are equivalent set-like lists.  That means checking two things.
    First, they must both be {i set-like}, meaning that they do not
    contain any duplicates.  Second, they must contain the same elements,
    though not necessarily in the same order. *)
let cmp_set_like_lists lst1 lst2 =
  let uniq1 = List.sort_uniq compare lst1 in
  let uniq2 = List.sort_uniq compare lst2 in
  List.length lst1 = List.length uniq1
  &&
  List.length lst2 = List.length uniq2
  &&
  uniq1 = uniq2

(** [pp_string s] pretty-prints string [s]. *)
let pp_string s = "\"" ^ s ^ "\""

(** [pp_list pp_elt lst] pretty-prints list [lst], using [pp_elt]
    to pretty-print each element of [lst]. *)
let pp_list pp_elt lst =
  let pp_elts lst =
    let rec loop n acc = function
      | [] -> acc
      | [h] -> acc ^ pp_elt h
      | h1::(h2::t as t') ->
        if n=100 then acc ^ "..."  (* stop printing long list *)
        else loop (n+1) (acc ^ (pp_elt h1) ^ "; ") t'
    in loop 0 "" lst
  in "[" ^ pp_elts lst ^ "]"

(* These tests demonstrate how to use [cmp_set_like_lists] and
   [pp_list] to get helpful output from OUnit. *)
let cmp_demo =
  [
    "order is irrelevant" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["foo"; "bar"] ["bar"; "foo"]);
    (* Uncomment this test to see what happens when a test case fails. *)
    (* "duplicates not allowed" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["foo"; "foo"] ["foo"]);  *)

  ]

(********************************************************************
   End helper functions.
 ********************************************************************)

(* You are welcome to add strings containing JSON here, and use them as the
   basis for unit tests.  Or you can add .json files in this directory and
   use them, too.  Any .json files in this directory will be included
   by [make zip] as part of your CMS submission. *)

let olin = Yojson.Basic.from_file "olin.json"
let statler = Yojson.Basic.from_file "statler.json"
let olin_adv = from_json olin
let statler_adv = from_json statler
let adventure_tests =
  [
    "test start room 1" >:: (fun _ ->
        assert_equal "lobby" (start_room olin_adv));
    "test start room 2" >:: (fun _ ->
        assert_equal "statler" (start_room statler_adv));
    "test room_id" >:: (fun _ ->
        assert_equal ["lobby";"libe";"basement";"stacks";"asian";"cubicles"] (room_ids olin_adv) ~cmp:cmp_set_like_lists  ~printer:(pp_list pp_string));
    "test description 1" >:: (fun _ ->
        assert_equal "You are in the lobby of Olin Library! You must find the perfect place to study for your upcoming 3110 prelim, but which room is it? You also need to find all of your study materials that are scattered throughout the library: your textbook, notebook, laptop, charger, pencil, and most importantly, that nice cup of coffee. You can go downstairs to the basement, upstairs to the stacks, straight ahead to reach the Asian Library, or go left to enter Libe Cafe."
          (description olin_adv "lobby"));
    "test description 2" >:: (fun _ ->
        assert_equal "You are back in the lobby. You hear the hushed gossip of students around you....You can go downstairs to the basement, upstairs to the stacks, straight ahead to reach the Asian Library, or go left to enter Libe Cafe."
          (description2 olin_adv "lobby"));
    "test exits 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["upstairs";"up";"elevator up";"elevator"] (exits olin_adv "basement"));
    "test exits 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["back"] (exits olin_adv "libe"));
    "test next_room 1" >:: (fun _ ->
        assert_equal "lobby"
          (next_room olin_adv "libe" "back"));
    "test next_room 2" >:: (fun _ ->
        assert_equal "lobby"
          (next_room olin_adv "basement" "upstairs"));
    "test next_room 3" >:: (fun _ ->
        assert_equal "basement"
          (next_room olin_adv "stacks" "elevator down"));
    "test next_room 4" >:: (fun _ ->
        assert_equal "macs"
          (next_room statler_adv "statler" "macs"));
    "test next_room 5" >:: (fun _ ->
        assert_equal "terrace"
          (next_room statler_adv "statler" "terrace"));
    "test next_room 6" >:: (fun _ ->
        assert_equal "terrace"
          (next_room statler_adv "macs" "north"));
    "test next_rooms 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["lobby"] (next_rooms olin_adv "libe"));
    "test next_rooms 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["cubicles";"lobby"] (next_rooms olin_adv "asian"));
    "test next_rooms 3" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
          ["asian"] (next_rooms olin_adv "cubicles"));
    "test treasure room 1" >:: (fun _ ->
        assert_equal false (is_treasure_room olin_adv "lobby"));
    "test treasure room 2" >:: (fun _ ->
        assert_equal true (is_treasure_room olin_adv "asian"));
    "test treasure room 3" >:: (fun _ ->
        assert_equal false (is_treasure_room olin_adv "libe"));
    "test treasure room 4" >:: (fun _ ->
        assert_equal false (is_treasure_room statler_adv "libe"));
    "test treasure room 5" >:: (fun _ ->
        assert_equal true (is_treasure_room statler_adv "statler"));
    "test get treasures 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
        ["textbook";"notebook";"coffee";"laptop charger";"laptop";"pencil"]
        (get_treasures olin_adv));
    "test get treasures 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists ~printer:(pp_list pp_string)
        ["salad";"flatbread";"chair"] (get_treasures statler_adv));
  ]

let command_tests =
  [
    "test parse 1" >:: (fun _ ->
        assert_equal (Go ["back"]) (parse "go back"));
    "test parse 2" >:: (fun _ -> assert_raises (Malformed) (fun() -> parse "GO"));
    "test parse 3" >:: (fun _ ->
        assert_equal (Take ["notebook"]) (parse "take notebook"));
    "test parse 4" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "Go elevator down"));
    "test parse 5" >:: (fun _ ->
        assert_equal (Quit) (parse "quit"));
    "test parse 6" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "quit game"));
    "test parse 7" >:: (fun _ ->
        assert_raises (Empty) (fun() -> parse " "));
    "test parse 8" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "Quit"));
    "test parse 9" >:: (fun _ ->
        assert_equal (Inventory) (parse "inventory"));
    "test parse 10" >:: (fun _ ->
        assert_equal (Drop(["pencil"])) (parse "drop      pencil"));
    "test parse 11" >:: (fun _ ->
        assert_equal (Unlock(["left"])) (parse "unlock left"));
    "test parse 12" >:: (fun _ ->
        assert_equal (Lock(["cubicles"])) (parse "lock cubicles"));
    "test parse 13" >:: (fun _ ->
        assert_equal (Score) (parse "score"));
    "test parse 14" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "score pizza"));
    "test parse 15" >:: (fun _ ->
        assert_raises (Malformed) (fun() -> parse "inventory pizza"));
  ]

let initial_olin = init_state olin_adv
let olin_state1 = get_state (go "left" olin_adv initial_olin)
let olin_state2 = get_state (take olin_adv olin_state1 "silver key")
let olin_state3 = get_state (go "back" olin_adv olin_state2)
let olin_state4 = get_state (drop olin_adv olin_state3 "silver key")
let initial_statler = init_state statler_adv
let statler_state1 = get_state (go "terrace" statler_adv initial_statler)
let statler_state2 = get_state (take statler_adv statler_state1 "chair")
let statler_state3 = get_state (take statler_adv statler_state2 "silver key")
let statler_state4 = get_state (take statler_adv statler_state3 "salad")
let state_tests =
  [
    "test current_room_id 1" >:: (fun _ ->
        assert_equal "lobby" (current_room_id initial_olin));
    "test current_room_id 2" >:: (fun _ ->
        assert_equal "libe" (current_room_id olin_state1));
    "test current_room_id 3" >:: (fun _ ->
        assert_equal "lobby" (current_room_id olin_state3));
    "test current_room_id 4" >:: (fun _ ->
        assert_equal "statler" (current_room_id initial_statler));
    "test current_room_id 5" >:: (fun _ ->
        assert_equal "terrace" (current_room_id statler_state1));
    "test current_room_id 6" >:: (fun _ ->
        assert_equal "terrace" (current_room_id statler_state2));
    "test visited 1" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists
        ["lobby"] (visited initial_olin));
    "test visited 2" >:: (fun _ ->
        assert_equal ~cmp:cmp_set_like_lists
        ["lobby"; "libe"] (visited olin_state2));
    "test get score 1" >:: (fun _ ->
        assert_equal 0 (get_score initial_olin));
    "test get score 2" >:: (fun _ ->
        assert_equal 8 (get_score olin_state1));
    "test inventory 1" >:: (fun _ ->
        assert_equal "" (inventory initial_olin));
    "test inventory 2" >:: (fun _ ->
        assert_equal "silver key" (inventory olin_state2));
    "test inventory 3" >:: (fun _ ->
        assert_equal "" (inventory statler_state1));
    "test inventory 4" >:: (fun _ ->
        assert_equal "chair" (inventory statler_state2));
    "test get items in room 1" >:: (fun _ ->
       assert_equal "" (get_items_in_room initial_olin));
    "test get items in room 2" >:: (fun _ ->
        assert_equal "silver key and coffee" (get_items_in_room olin_state1));
    "test get items in room 3" >:: (fun _ ->
        assert_equal "coffee" (get_items_in_room olin_state2));
    "test get items in room 4" >:: (fun _ ->
        assert_equal "silver key" (get_items_in_room olin_state4));
    "test get items in room 5" >:: (fun _ ->
       assert_equal "" (get_items_in_room initial_statler));
    "test get items in room 6" >:: (fun _ ->
       assert_equal "salad and silver key" (get_items_in_room statler_state2));
    "test get items in room 7" >:: (fun _ ->
       assert_equal "salad" (get_items_in_room statler_state3));
    "test get items in room 8" >:: (fun _ ->
       assert_equal "" (get_items_in_room statler_state4));
  ]

let suite =
  "test suite for A2"  >::: List.flatten [
    adventure_tests;
    command_tests;
    state_tests;
  ]

let _ = run_test_tt_main suite
